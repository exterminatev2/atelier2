
package provider;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.boundary.PartieRessource;
import org.entity.Partie;

@Securise
@Provider
public class AuthentificationFiltre implements ContainerRequestFilter {
     
   @Inject 
   PartieRessource ressource;
    
   
    @Override
    public void filter(ContainerRequestContext requestContext) {
        String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        
        if(authHeader == null || !authHeader.startsWith("Bearer")){
            throw new NotAuthorizedException("Probleme header authorization");
        }
        //on extrait le token et on le verifie
        String token = authHeader.substring("Bearer".length()).trim();
        
        try{
            valideToken(token);
        }catch (Exception e){
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
        
    }
    
    
    private void valideToken(String token){
        List<Partie> lp =  this.ressource.findPartie(token);
        if(lp.size() < 1){
            throw new ForbiddenException("Token invalide");
        }
    }
}
