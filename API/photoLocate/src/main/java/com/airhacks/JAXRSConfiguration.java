package com.airhacks;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.boundary.PartieRepresentation;
import org.boundary.PhotoRepresentation;
import org.boundary.SerieRepresentation;
import org.boundary.AuthentificationBoundary;
import org.boundary.UploadService;
import provider.AuthentificationFiltre;
import provider.CORSRequestFilter;
import provider.CORSResponseFilter;

/**
 * Configures a JAX-RS endpoint. Delete this class, if you are not exposing
 * JAX-RS resources in your application.
 *
 * @author airhacks.com
 */
@ApplicationPath("api")
public class JAXRSConfiguration extends Application {
    
    @Override
    public Set<Class<?>> getClasses(){
        Set<Class<?>> s = new HashSet<>();
        //charger toutes les classes avec un path
      
        s.add(UploadService.class);
        s.add(PartieRepresentation.class);
        s.add(PhotoRepresentation.class);
        s.add(SerieRepresentation.class);
        
        s.add(AuthentificationBoundary.class);
        s.add(AuthentificationFiltre.class);
        
        s.add(CORSRequestFilter.class);
        s.add(CORSResponseFilter.class);
        
        s.add(org.glassfish.jersey.filter.LoggingFilter.class);
        s.add(org.glassfish.jersey.media.multipart.MultiPartFeature.class);
         
    
        return s;
    }
}

