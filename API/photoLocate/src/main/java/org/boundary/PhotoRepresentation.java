    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;


import java.io.IOException;
import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import org.entity.Photo;

/**
 *
 * @author Kevin , Anthony
 */
@Stateless
@Path("photos")
public class PhotoRepresentation {
    
    @Inject
    PhotoRessource ressource;
    
   
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response all(@Context UriInfo uriInfo) throws IOException {
        List<Photo> res = this.ressource.findAll();
        for (int i=0; i< res.size(); i++){
            /*if(res.get(i).links.size() < 1){
               res.get(i).addLink("self",getUriForGet(uriInfo,res.get(i)));
            }*/
        }
         ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
         String json = ow.writeValueAsString(res);
        return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    

    
    
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("{id}")
    public Response find(@PathParam("id") Integer id, @Context UriInfo uriInfo) throws IOException {
       Photo p = this.ressource.findById(id);
        if (p != null) {
            /*if(p.links.size() < 1){
                p.addLink("self",getUriForGet(uriInfo,p));
            }*/
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(p);
            return Response.ok(json, MediaType.APPLICATION_JSON).build();
       } else {
            return Response.status(Response.Status.NOT_FOUND).build();
       }
    }   
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response save(Photo photo, @Context UriInfo info) {
       
        Photo saved = this.ressource.save(photo);
        Integer id = saved.getIdPhoto();
          
        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).entity(saved).build();
    } 
  
    
    private String getUriForGet(UriInfo uriInfo, Photo s){
        String id = Integer.toString(s.getIdPhoto());
        String uri = uriInfo.getBaseUriBuilder().path(PhotoRepresentation.class).path(id).build().toString();
        return uri;
    }
}
