package org.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.entity.Partie;

@Stateless
public class PartieRessource {

    @PersistenceContext
    EntityManager em;

    public Partie save(Partie part) {
        Partie res = this.em.merge(part);
        this.em.flush();
        return res;
    }
    
    public List<Partie> findFinished(String status){
        return (List<Partie>) this.em.createNamedQuery("Partie.findByStatus").setParameter("status", status).setMaxResults(5).getResultList();
    }
    
  
    
    public List<Partie> findPartie(String token){
        //System.out.println(this.em.createNamedQuery("Partie.findByToken").setParameter("token",token));
        return  (List<Partie>) this.em.createNamedQuery("Partie.findByToken").setParameter("token",token).getResultList();
     
    }
     
 
    

}
