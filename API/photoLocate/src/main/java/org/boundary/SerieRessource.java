/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.entity.Photo;
import org.entity.Serie;

/**
 *
 * @author Anthony
 */
@Stateless
public class SerieRessource {
    @PersistenceContext
    EntityManager em;   
    
    public Serie findById(Integer id){
        return this.em.find(Serie.class, id);
    }
    
    public List<Serie> findAll() {
      return this.em.createNamedQuery("sfindAll",Serie.class).getResultList();
    }
    
    public List<Photo> findPhotos(Integer id){
        return this.em.createNamedQuery("pfindByIdSerie").setParameter("idSerie",id).getResultList();
    }
    
    
    
}
