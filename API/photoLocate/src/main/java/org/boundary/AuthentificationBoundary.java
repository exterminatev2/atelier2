package org.boundary;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ejb.Stateless;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.entity.Partie;

@Stateless
@Path("authentification")
public class AuthentificationBoundary {
    
   
    @GET
    @Produces("application/json") //pour le token
    public Response authentificationUtilisateur(){
        try{
            String token = generationToken();
            JsonObject jToken = Json.createObjectBuilder().
                    add("token", token).
                    build();
            return Response.ok(jToken).build();
        }catch (Exception e){
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }
    
   
    

    public String generateSalt(int length) {
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        String pass = "";
        for(int x=0;x<length;x++)   {
           int i = (int)Math.floor(Math.random() * chars.length() -1);
           pass += chars.charAt(i);
        }
        return pass;
    }


    private String generationToken() throws NoSuchAlgorithmException{

        // le salt est g?n?r? et stock? dans la BD
        String salt = this.generateSalt(12);
        String token = salt;
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(token.getBytes());
        byte byteData[] = md.digest();
        //convertit en String
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < byteData.length; i++){
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16 ).substring(1));
        }
        return sb.toString();
    }
}