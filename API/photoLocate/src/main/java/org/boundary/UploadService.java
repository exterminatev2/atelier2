package org.boundary;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("file")
public class UploadService {
    
    @POST
    @Path("upload/idserie={idserie}/idphoto={idphoto}")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    public Response uploadFichier(
        @FormDataParam("file") InputStream uploadInputStream, 
        @FormDataParam("file") FormDataContentDisposition fileDetail,
        @PathParam("idserie") Integer idserie,
        @PathParam("idphoto") Integer idphoto
    ){
   
        
        String uploadFileLocation = "C:\\wamp\\www\\privee\\Atelier_2\\atelier2\\Client\\img\\series\\serie-"+idserie+"\\photo_"+idphoto+".jpg";
        writeToFile(uploadInputStream, uploadFileLocation);
        String output = "Fichier disponible" + uploadFileLocation;
        return Response.status(200).entity(output).build();
    }
    
    
    public void writeToFile(InputStream uploadInputStream, String uploadFileLocation){
        try{
          
            int read = 0;
            byte[] bytes = new byte[1024];
            OutputStream out = new FileOutputStream(new File(uploadFileLocation));
            while((read = uploadInputStream.read(bytes))!= -1){
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        }catch (Exception ioe){
            ioe.printStackTrace();
        }
    }
    
}