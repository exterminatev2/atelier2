/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.entity.Photo;

/**
 *
 * @author Anthony
 */
@Stateless
public class PhotoRessource {
    @PersistenceContext
    EntityManager em;   
    
    public Photo findById(Integer id){
        return this.em.find(Photo.class, id);
    }
    
    public List<Photo> findAll() {
      return this.em.createNamedQuery("pfindAll",Photo.class).getResultList();
    }
    
    
    
    public Photo save(Photo pho) {
        Photo res = this.em.merge(pho);
        this.em.flush();
        return res;
    }
 
    
}
