    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;


import java.io.IOException;
import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.entity.Partie;
import org.entity.Photo;
import provider.Securise;

/**
 *
 * @author Kevin , Anthony
 */
@Stateless
@Path("parties")
public class PartieRepresentation {
    
   @Inject 
   PartieRessource ressource;
      
    @GET
    @Path("status={status}")
    public Response findByFinished(@PathParam("status") String status) throws IOException{
        List<Partie> p = this.ressource.findFinished(status);
        
        if(p.size() < 1  ){
               
             return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(p);
            return Response.ok(json,MediaType.APPLICATION_JSON).build();
        }
    }
   
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(Partie part, @Context UriInfo info) {
        System.out.println(part);
        Partie saved = this.ressource.save(part);
        Integer id = saved.getIdPartie();
        
        URI uri = info.getAbsolutePathBuilder().path("/" + id).build();
        return Response.created(uri).build();
    } 
    
    
    @GET
    @Path("{token}")
    @Securise
    public Response findByToken(@PathParam("token") String token) throws IOException{
        
        List<Partie> p = this.ressource.findPartie(token);
        if(p.size() < 1  ){
               
             return Response.status(Response.Status.NOT_FOUND).build();
        } else {
             ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(p.get(0));
            return Response.ok(json,MediaType.APPLICATION_JSON).build();
        }
    }
  
    
    @PUT
    @Path("{id}")
    @Produces("application/json")
    @Securise
    public Partie finishPartie(@PathParam("id") Integer id, Partie part){
        part.setIdPartie(id);
        return this.ressource.save(part);
       
    }
    

    private String getUriForGet(UriInfo uriInfo, Photo s){
        String id = Integer.toString(s.getIdPhoto());
        String uri = uriInfo.getBaseUriBuilder().path(PartieRepresentation.class).path(id).build().toString();
        return uri;
    }
}
