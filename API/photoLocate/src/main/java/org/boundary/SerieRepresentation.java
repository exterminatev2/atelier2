    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.boundary;


import java.io.IOException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.entity.Photo;
import org.entity.Serie;

/**
 *
 * @author Kevin , Anthony
 */
@Stateless
@Path("series")
public class SerieRepresentation {
    
    @Inject
    SerieRessource ressource;
    
     /**
     * Liste des series
     *
     * @param uriInfo
     * @return  object en json 
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response all(@Context UriInfo uriInfo) throws IOException {
        
        List<Serie> res = this.ressource.findAll();
         for (int i=0; i< res.size(); i++){
            /*if(res.get(i).links.size() < 1){
               res.get(i).addLink("self",getUriForSerie(uriInfo,res.get(i)));
            }*/
            
         }
           ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
           String json = ow.writeValueAsString(res);
         return Response.ok(json, MediaType.APPLICATION_JSON).build();
    }
    
    /**
     * Liste des photo d'une serie
     *
     * @param id de la serie
     * @param uriInfo
     * @return  Objet en Json 
     */
    @GET
    @Path("{id}/photos")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findPhotos(@PathParam("id") Integer id, @Context UriInfo uriInfo) throws IOException {
        List<Photo> res = this.ressource.findPhotos(id);
        Serie s = new Serie(id);
        for (int i=0; i< res.size(); i++){
            /*if(res.get(i).links.size() < 1){
               res.get(i).addLink("self",getUriForPhotosSerie(uriInfo,s));
                res.get(i).addLink("self",getUriForPhoto(uriInfo,res.get(i).getIdPhoto()));
            }*/
        }
        
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
         String json = ow.writeValueAsString(res);
        return Response.ok(json, MediaType.APPLICATION_JSON).build();
        
    }
    
      /**
     * Detail d'une serie
     *
     * @param id de la serie
     * @param uriInfo
     * @return  La r�ponse de la requ�te 
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{id}")
    public Response find(@PathParam("id") Integer id, @Context UriInfo uriInfo) throws IOException {
       Serie s = this.ressource.findById(id);
       if (s != null) {
            /*if(s.links.size() < 1){
                s.addLink("self",getUriForSerie(uriInfo,s));
            }*/
            
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(s);
            return Response.ok(json, MediaType.APPLICATION_JSON).build();
       } else {
            return Response.status(Response.Status.NOT_FOUND).build();
       }
    }   
    
    private String getUriForSerie(UriInfo uriInfo, Serie s){
        String id = Integer.toString(s.getIdSerie());
        String uri = uriInfo.getBaseUriBuilder().path(SerieRepresentation.class).path(id).build().toString();
        return uri;
    }
    
    private String getUriForPhoto(UriInfo uriInfo, Integer idPhoto){
        String id = Integer.toString(idPhoto);
        String uri = uriInfo.getBaseUriBuilder().path(PhotoRepresentation.class).path(id).build().toString();
        return uri;
    }
    
        private String getUriForPhotosSerie(UriInfo uriInfo, Serie s){
        String id = Integer.toString(s.getIdSerie());
        String uri = uriInfo.getBaseUriBuilder().path(SerieRepresentation.class).path(id).path(PhotoRepresentation.class).build().toString();
        return uri;
    }
}
