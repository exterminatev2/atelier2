/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *s
 * @author Kevnfs
 */
@Entity
@Table(name = "Serie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "sfindAll", query = "SELECT s FROM Serie s"),
    @NamedQuery(name = "sfindByVille", query = "SELECT s FROM Serie s WHERE s.ville = :ville"),
    @NamedQuery(name = "sfindByLatitude", query = "SELECT s FROM Serie s WHERE s.latitude = :latitude"),
    @NamedQuery(name = "sfindByLongitude", query = "SELECT s FROM Serie s WHERE s.longitude = :longitude"),
    @NamedQuery(name = "sfindByDistance", query = "SELECT s FROM Serie s WHERE s.distance = :distance"),
    @NamedQuery(name = "sfindAllVille", query = "SELECT s.idSerie, s.ville, s.latitude, s.longitude FROM Serie s"),

})
public class Serie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_serie")
    private Integer idSerie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ville")
    private String ville;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private double longitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "distance")
    private int distance;
    /*
    @Transient
    @XmlElement(name="_links")
    public List<Link> links = new ArrayList();
*/

    public Serie() {
    }

    public Serie(Integer idSerie) {
        this.idSerie = idSerie;
    }
    
     public Serie(Integer idSerie, String ville) {
        this.idSerie = idSerie;
        this.ville = ville;
    }
     
    public Serie(Integer idSerie, String ville, double latitude, double longitude, int distance) {
        this.idSerie = idSerie;
        this.ville = ville;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }

    public Integer getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(Integer idSerie) {
        this.idSerie = idSerie;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSerie != null ? idSerie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Serie)) {
            return false;
        }
        Serie other = (Serie) object;
        if ((this.idSerie == null && other.idSerie != null) || (this.idSerie != null && !this.idSerie.equals(other.idSerie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.entity.Serie[ idSerie=" + idSerie + " ; Ville="+ ville+ "]";
    }
    /*
    public void addLink(String rel, String uri){
        this.links.add(new Link(rel, uri));
    }
    
    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }*/
    

    
}
