/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevnfs
 */
@Entity
@Table(name = "partie")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries({
    @NamedQuery(name = "findAll", query = "SELECT p FROM Partie p"),
    @NamedQuery(name = "findByIdPartie", query = "SELECT p FROM Partie p WHERE p.idPartie = :idPartie"),
    @NamedQuery(name = "Partie.findByToken", query = "SELECT p FROM Partie p WHERE p.token = :token"),
    @NamedQuery(name = "Partie.findByNbPhotos", query = "SELECT p FROM Partie p WHERE p.nbPhotos = :nbPhotos"),
    @NamedQuery(name = "findByScore", query = "SELECT p FROM Partie p WHERE p.score = :score"),
    @NamedQuery(name = "findByJoueur", query = "SELECT p FROM Partie p WHERE p.joueur = :joueur"),
    @NamedQuery(name = "findByIdSerie", query = "SELECT p FROM Partie p WHERE p.idSerie = :idSerie"),
    @NamedQuery(name = "Partie.findByStatus", query = "SELECT p FROM Partie p WHERE p.status = :status order by p.score desc")
})
public class Partie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_partie")
    private Integer idPartie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "token")
    private String token;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nb_photos")
    private int nbPhotos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "score")
    private int score;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "joueur")
    private String joueur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_serie")
    private int idSerie;
/*
    @Transient
    @XmlElement(name = "_links")
    private List<Link> links = new ArrayList();

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }*/

    public Partie() {
    }

    public Partie(Integer idPartie) {
        this.idPartie = idPartie;
    }

    public Partie(String token, int nbPhotos, int score, String joueur, String status, int idSerie) {
        this.token = token;
        this.nbPhotos = nbPhotos;
        this.score = score;
        this.joueur = joueur;
        this.status = status;
        this.idSerie = idSerie;
    }

    public Partie(Integer idPartie, String token, int nbPhotos, int score, String joueur, String status, int idSerie) {
        this.idPartie = idPartie;
        this.token = token;
        this.nbPhotos = nbPhotos;
        this.score = score;
        this.joueur = joueur;
        this.status = status;
        this.idSerie = idSerie;
    }

    public Integer getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(Integer idPartie) {
        this.idPartie = idPartie;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getNbPhotos() {
        return nbPhotos;
    }

    public void setNbPhotos(int nbPhotos) {
        this.nbPhotos = nbPhotos;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getJoueur() {
        return joueur;
    }

    public void setJoueur(String joueur) {
        this.joueur = joueur;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(int idSerie) {
        this.idSerie = idSerie;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPartie != null ? idPartie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partie)) {
            return false;
        }
        Partie other = (Partie) object;
        if ((this.idPartie == null && other.idPartie != null) || (this.idPartie != null && !this.idPartie.equals(other.idPartie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.entity.Partie[ idPartie=" + idPartie + " ]";
    }
/*      void addLink(String rel, String uri) {
        this.links.add(new Link(rel, uri));
    }
*/
}
