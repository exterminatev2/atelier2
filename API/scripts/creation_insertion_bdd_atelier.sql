-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 06 Février 2016 à 10:12
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `atelier`
--

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE IF NOT EXISTS `partie` (
  `id_partie` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nb_photos` int(3) NOT NULL,
  `score` int(10) NOT NULL,
  `joueur` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_serie` int(11) NOT NULL,
  PRIMARY KEY (`id_partie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=270 ;

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id_photo` int(11) NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `id_serie` int(11) NOT NULL,
  PRIMARY KEY (`id_photo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`id_photo`, `description`, `latitude`, `longitude`, `url`, `id_serie`) VALUES
(1, 'IUT Nancy Charlemagne', 48.6828361, 6.15894, 'photo_1.jpg', 1),
(2, 'Place Stanislas', 48.6936184, 6.1810473, 'photo_2.jpg', 1),
(3, 'Gare SNCF', 48.6898379, 6.1722542, 'photo_3.jpg', 1),
(4, 'Notre Dame de l''Annonciation', 48.6911853, 6.1839242, 'photo_4.jpg', 1),
(5, 'Centre commercial Saint Sébastien', 48.687957, 6.17901, 'photo_5.jpg', 1),
(6, 'Cinéma Kinépolis', 48.69156, 6.193396, 'photo_6.jpg', 1),
(7, 'C.H.U Nancy-Brabois', 48.6476936, 6.1474528, 'photo_7.jpg', 1),
(8, 'Parc de la Pépinière', 48.6915596, 6.1868031, 'photo_8.jpg', 1),
(9, 'Place Carnot', 48.6936659, 6.1758218, 'photo_9.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `serie`
--

CREATE TABLE IF NOT EXISTS `serie` (
  `id_serie` int(11) NOT NULL AUTO_INCREMENT,
  `ville` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `distance` int(11) NOT NULL,
  PRIMARY KEY (`id_serie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `serie`
--

INSERT INTO `serie` (`id_serie`, `ville`, `latitude`, `longitude`, `distance`) VALUES
(1, 'Nancy', 48.6880796, 6.1558845, 500),
(2, 'Metz', 49.1047194, 6.1284259, 500);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
