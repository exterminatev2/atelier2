app.factory('Partie', ['$resource', function ($resource) {

    return $resource(window.ws + 'parties/:paid', {paid: '@paid'}, {
        update: {method: 'PUT'}
    });
}]);

app.factory('PartieStatus', ['$resource', function ($resource) {

    return $resource(window.ws + 'parties/status=:state', {state: '@state'}, {
        update: {method: 'PUT'}
    });
}]);

app.factory('Photo', ['$resource', function ($resource) {
    return $resource(window.ws + 'photos/:phid', {phid: '@phid'}, {
        save: {method: 'POST', headers: {'Content-Type': 'application/json'}},
        update: {method: 'PUT'}
    });

}]);

app.factory('Serie', ['$resource', function ($resource) {
    return $resource(window.ws + 'series/:sid', {sid: '@sid'}, {
        save: {method: 'POST', headers: {'Content-Type': 'application/json'}},
        delete: {method: 'DELETE'},
        update: {method: 'PUT'}
    });

}]);

app.factory('SeriePhoto', ['$resource', function ($resource) {
    return $resource(window.ws + 'series/:sid/photos', {sid: '@sid'}, {
        save: {method: 'POST', headers: {'Content-Type': 'application/json'}},
        delete: {method: 'DELETE'},
        update: {method: 'PUT'}
    });

}]);

app.factory('Auth', ['$resource', function ($resource) {
    return $resource(window.ws + 'authentification');
}]);


app.factory('UploadPhoto', ['$resource', function ($resource) {
    return $resource(window.ws + 'file/upload/?dossier=:dossier', {dossier: '@dossier'}, {
            save: {method: 'POST', headers: {'Content-Type': undefined}}
        }
    );
}]);