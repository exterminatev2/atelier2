app.controller('FinishController', ['$scope', '$http', '$location', '$cookies', '$rootScope', '$http', 'Partie', 'PartieStatus',
    function ($scope, $http, $location, $cookies, $rootScope, $http, Partie, PartieStatus) {

        $http.defaults.headers.common.Authorization = "Bearer " + $rootScope.tokenFinish;

        var finishLoc = this;
        finishLoc.scoreFinal = $rootScope.scoreFinal;

        finishLoc.putScore = function () {
            finishLoc.putPartie = Partie.get({paid: $rootScope.tokenFinish}, function (successGet) {

                successGet.token = $rootScope.tokenFinish;
                successGet.joueur = $scope.share.pseudo;
                successGet.idSerie = $rootScope.idSerie;
                successGet.status = "Finie";
                successGet.score = $rootScope.scoreFinal;

                finishLoc.putPartie = finishLoc.putPartie.$update({paid: successGet.idPartie}, function (successPut) {
                    getPartieFini();
                }, function (error) {
                    console.log(error);
                });
            }, function (error) {
                console.log(error);
            });
        };

        function getPartieFini() {
            PartieStatus.query({state: "Finie"}, function (result) {
                $scope.listTop = result;
            }, function (error) {
                console.log(error)
            });
        }
        getPartieFini();
    }]);
