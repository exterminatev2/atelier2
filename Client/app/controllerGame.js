// Controleur Game
app.controller('GameController', ['$scope', '$http', '$location', '$cookies', '$rootScope', '$timeout', 'ngAudio',
    function ($scope, $http, $location, $cookies, $rootScope, $timeout, ngAudio) {

        var gameLoc = this;
        gameLoc.counterBegin = 5;
        gameLoc.counterStep = 10;
        gameLoc.clickedMap = 0;
        gameLoc.sound = '';
        gameLoc.clicked = false;
        gameLoc.dist = 0;
        gameLoc.timeClicked = 0;
        gameLoc.counterScore = 4;
        gameLoc.scoreTotal = 0;

        gameLoc.token = $rootScope.tokenGame;

        // Game params
        gameLoc.idSerie = 0;
        gameLoc.nbCurrentPicture = 0;
        gameLoc.nbPictures = 5;
        gameLoc.scoreStep = 0;
        gameLoc.currentTitle = '';
        gameLoc.currentUrl = '';

        // Game params carte
        gameLoc.lat = $rootScope.latitude;
        gameLoc.long = $rootScope.longitude;
        gameLoc.distance = $rootScope.distance;
        gameLoc.photos = new Array();

        //Carte
        gameLoc.markers = new Array();
        angular.extend($scope, {
            city: {
                lat: gameLoc.lat,
                lng: gameLoc.long,
                zoom: 13
            },
            tiles: {
                url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            },
            events: {}
        });

        // Modal begin game
        gameLoc.timeOutBegin = function () {
            // End of countdown
            if (gameLoc.counterBegin > 0) {
                gameLoc.counterBegin--;
                timeout = $timeout(gameLoc.timeOutBegin, 1000);
            }

            else if (gameLoc.counterBegin == 0) {
                $('#modal1').closeModal();

                // Affiche image serie
                gameLoc.currentTitle = gameLoc.photos[gameLoc.nbCurrentPicture].description;
                gameLoc.currentUrl = "img/series/serie-" + gameLoc.idSerie + "/photo_"
                    + gameLoc.photos[gameLoc.nbCurrentPicture].idPhoto + ".jpg";
                $('#images-series').openModal({dismissible: false});

                gameLoc.loadImage();
            }
        }

        gameLoc.getSeriePhotos = function () {
            var req = {
                method: 'GET',
                url: window.ws + "series/" + gameLoc.idSerie + "/photos"
            };

            $http(req)
                .success(function (data, status, headers, config) {
                    gameLoc.initPhotosArray(data);
                })
                .error(function (data, status, headers, config) {
                });
        }

        gameLoc.initPhotosArray = function (datas) {
            var data_Length = Object.keys(datas).length;
            var picker = pick(gameLoc.nbPictures, 0, data_Length - 1);

            angular.forEach(picker, function (value, key) {
                gameLoc.photos.push(datas[value]);
            });
        };

        gameLoc.initQuery = function () {
            // Game params
            gameLoc.idSerie = $rootScope.datas.idSerie;
            gameLoc.getSeriePhotos();
        }

        gameLoc.startGame = function () {
            gameLoc.initQuery();
            $timeout(gameLoc.timeOutBegin, 1000);
        }

        gameLoc.loadImage = function () {
            setTimeout(function () {
                // Hide picture
                $('#images-series').closeModal();
                $('.lean-overlay').remove();

                $scope.$apply();

                // Load Step
                gameLoc.startGameStep();
            }, 6000);
        }


        // Counter Step
        $width = 0;
        gameLoc.gameStep = function () {
            if (gameLoc.counterStep > 0) {
                gameLoc.counterStep--;
                timeout = $timeout(gameLoc.gameStep, 1000);

                $width++;
                $('.determinate').css({'width': ($width * 10) + '%'});

                if ($width == 4) {
                    // jaune
                    if (gameLoc.clicked === false) {
                        $('.progress').css({'background-color': '#fff59d'});
                        $('.determinate').css({'background-color': '#ffc107'});
                    }
                }
                else if ($width == 5) {
                    if (gameLoc.clicked === false) {
                        gameLoc.sound = ngAudio.load("audio.mp3");
                        gameLoc.sound.loop = true;
                        gameLoc.sound.play();
                    }
                }

                else if ($width == 6) {
                    // orange
                    if (gameLoc.clicked === false) {
                        $('.progress').css({'background-color': '#ffcc80'});
                        $('.determinate').css({'background-color': '#ff9800'});
                        gameLoc.sound.playbackRate = 1.2;
                    }
                }

                else if ($width == 8) {
                    // rouge
                    if (gameLoc.clicked === false) {
                        $('.progress').css({'background-color': '#ffcdd2'});
                        $('.determinate').css({'background-color': '#f44336'});
                        gameLoc.sound.playbackRate = 1.4;
                    }
                }

                // Finish
                else if ($width == 10) {
                    if (gameLoc.clicked === false) {
                        gameLoc.sound.stop();
                    }
                }

                gameLoc.loadMap();
            }
            else if (gameLoc.counterStep == 0) {
                //Calcule du score
                if (gameLoc.markers.length == 1) {
                    gameLoc.scoreStep = calculScore(gameLoc.distance, gameLoc.dist, gameLoc.timeClicked);
                    gameLoc.scoreTotal += gameLoc.scoreStep;
                }

                //Ouverture modal score
                $('#modalScore').openModal({dismissible: false});

                gameLoc.startScoreTimer();
            }
        };

        gameLoc.startGameStep = function () {
            $timeout(gameLoc.gameStep, 1000);
        }


        // Load maps
        gameLoc.loadMap = function () {

            $scope.$on("leafletDirectiveMap.click", function (event, args) {
                var leafEvent = args.leafletEvent;
                var timeClicked = 0;
                gameLoc.clickedMap++;

                if (gameLoc.clickedMap == 1) {

                    if (gameLoc.markers.length == 0) {
                        gameLoc.markers.push({
                            lat: leafEvent.latlng.lat,
                            lng: leafEvent.latlng.lng
                        });

                        timeClicked = 10 - gameLoc.counterStep;
                        gameLoc.timeClicked = timeClicked;

                        gameLoc.counterStep = 0;
                        $('.determinate').css({'width': '100%'});
                        gameLoc.clicked = true;
                    }

                    // Calculate distance
                    if (gameLoc.markers.length == 1) {
                        var dist = distance(gameLoc.markers[0].lat, gameLoc.markers[0].lng,
                            gameLoc.photos[gameLoc.nbCurrentPicture].latitude,
                            gameLoc.photos[gameLoc.nbCurrentPicture].longitude, "K");
                        dist = Math.round(dist * 1000);
                        gameLoc.dist = dist;
                    }
                }
            });
        }

        gameLoc.nextPicture = function () {
            // Function to next picture
            if (gameLoc.nbCurrentPicture < gameLoc.nbPictures - 1) {

                // New vars
                gameLoc.currentTitle = gameLoc.photos[gameLoc.nbCurrentPicture + 1].description;
                gameLoc.currentUrl = "img/series/serie-" + gameLoc.idSerie + "/photo_"
                    + gameLoc.photos[gameLoc.nbCurrentPicture + 1].idPhoto + ".jpg";

                $('#images-series').openModal({dismissible: false});

                // Re-init vars
                gameLoc.markers = new Array();
                gameLoc.counterBegin = 0;
                gameLoc.counterStep = 10;
                $width = 0;
                gameLoc.clickedMap = 0;
                gameLoc.clicked = false;
                gameLoc.counterScore = 4;
                $('.progress').css({'background-color': '#acece6'});
                $('.determinate').css({'background-color': '#26a69a'});

                $('#images-series').openModal({dismissible: false});
                gameLoc.nbCurrentPicture++;
                gameLoc.loadImage();

            }
            else if (gameLoc.nbCurrentPicture >= gameLoc.nbPictures - 1) {
                $rootScope.scoreFinal = gameLoc.scoreTotal;
                $rootScope.tokenFinish = gameLoc.token;
                $rootScope.idSerie = gameLoc.idSerie;
                $location.path("/finish");
            }
        };

        //Timer score modal
        gameLoc.timerScore = function () {
            if (gameLoc.counterScore > 0) {
                gameLoc.counterScore--;
                timeout = $timeout(gameLoc.timerScore, 1000);
            }
            else if (gameLoc.counterScore == 0) {
                $('#modalScore').closeModal();

                // Next picture
                gameLoc.nextPicture();
            }
        }

        gameLoc.startScoreTimer = function () {
            $timeout(gameLoc.timerScore, 1000);
        }
    }]);


function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180;
    var radlat2 = Math.PI * lat2 / 180;
    var theta = lon1 - lon2;
    var radtheta = Math.PI * theta / 180;
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    if (unit == "K") {
        dist = dist * 1.609344;
    }
    if (unit == "N") {
        dist = dist * 0.8684;
    }
    return dist;
}

function calculScore(D, distance, temps) {
    var score = 0;
    if (temps < 10) {
        // Distance
        if (distance <= D) {
            score += 5;
        } else if (distance <= 2 * D) {
            score += 3;
        } else if (distance < 3 * D) {
            score = +1;
        }

        if (temps < 2) {
            score *= 4;
        }
        else if (temps < 5) {
            score *= 2;
        }
    }
    return score;
}

function pick(n, min, max) {
    var values = [], i = max;
    while (i >= min) values.push(i--);
    var results = [];
    var maxIndex = max;

    for (i = 1; i <= n; i++) {
        maxIndex--;
        var index = Math.floor(maxIndex * Math.random());
        results.push(values[index]);
        values[index] = values[maxIndex];
    }
    return results;
}