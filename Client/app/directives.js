app.directive("serie", ['$http', '$location', 'Auth', 'Partie', '$timeout', '$rootScope', 'Serie',
    function ($http, $location, Auth, Partie, $timeout, $rootScope, Serie) {

        return {
            restrict: 'E',
            templateUrl: 'templates/serie.html',
            link: function (scope, element, attrs) {
                scope.game = function (idSerie) {

                    Serie.get({sid: idSerie}, function (serie) {
                        $rootScope.latitude = serie.latitude;
                        $rootScope.longitude = serie.longitude;
                        $rootScope.distance = serie.distance;
                    }, function (error) {
                    });

                    var req = {
                        method: 'GET',
                        url: window.ws + "authentification"
                    };

                    $http(req)
                        .success(function (data, status, headers, config) {
                            datas = {
                                "token": data.token,
                                "nbPhotos": 5,
                                "score": 0,
                                "joueur": "sf",
                                "status": "creee",
                                "idSerie": idSerie
                            };

                            Partie.save(datas, function (success) {
                                $rootScope.datas = datas;
                                $rootScope.tokenGame = data.token;
                                $location.path("/game");
                            }, function (error) {
                            });
                        })
                        .error(function (data, status, headers, config) {
                        });
                }
            }
        }
    }]);

app.directive("topgamer", function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/topGamer.html'
    }
});
