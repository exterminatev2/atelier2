// Les controleurs
app.controller('SeriesController', ['Serie', '$scope', '$http', '$location', '$cookies', '$rootScope',
    function (Serie, $scope, $http, $location, $cookies, $rootScope) {
        var seriesLoc = this;

        // Fonction qui permet de recuperer les series
        function getSeries() {
            Serie.query(function (series) {
                $scope.listSeries = series;
            }, function (error) {
                console.log(error);
            });
        }

        getSeries();
    }]);