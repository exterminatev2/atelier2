app.controller('AdminController', ['$scope', '$http', '$location', '$cookies', '$rootScope', 'Serie', 'Photo', 'SeriePhoto', 'UploadPhoto', 'Upload', '$timeout',
    function ($scope, $http, $location, $cookies, $rootScope, Serie, Photo, SeriePhoto, UploadPhoto, Upload, $timeout) {

        $scope.connected = true;
        $scope.show = false;
        $scope.image = {
            "description": "",
            "latitude": 0.0,
            "longitude": 0.0,
            "url": "",
            "idSerie": 0

        };
        $scope.idamodif;
        $scope.newserie = [];

        function getSeries() {
            Serie.query(function (series) {
                $scope.listSeries = series;
            }, function (error) {
                console.log(error);
            });
        }


        getSeries();

        // affiche le form pour ajouter une image a la série
        $scope.showForm = function (id_serie) {
            $scope.show = true;
            $scope.idamodif = id_serie;

            SeriePhoto.query({sid: id_serie}, function (photos) {
                $scope.nb_img = photos.length;
            }, function (error) {
                console.log(error);
            });


        }

        $scope.uploadFiles = function (file, errFiles) {
            $scope.f = file;
            $scope.errFile = errFiles && errFiles[0];
            if (file) {
                file.upload = Upload.upload({
                    url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        file.result = response.data;
                    });
                });
                $scope.f = file;
            }
        }

        $scope.addImage = function () {
            $scope.image.idSerie = $scope.idamodif;
            var test = $scope.f;
            Photo.save($scope.image, function (success) {
                var formData = new FormData();
                
                test.idSerie = $scope.idamodif;
                formData.append("file", test);
              

                var req = {
                    method: 'POST',
                    url: "http://10.10.189.70:8080/photoLocate/api/file/upload/idserie=" + $scope.idamodif + "/idphoto=" + success.idPhoto,
                    data: formData,
                    headers: {'Content-Type': undefined}
                };

                $http(req)
                    .success(function (data, status, headers, config) {
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                    });
                     $scope.f = "";
                $scope.image = {
                "description": "",
                "latitude": 0.0,
                "longitude": 0.0,
                "url": "",
                "idSerie": 0

            };
            }, function (error) {
                console.log(error);
            });

           

            $scope.nb_img++;
        };

        $scope.addGame = function () {
            // POST
            $scope.listSeries.push($scope.newserie);
            $scope.newserie = [];

        };

        $scope.hideAddImg = function () {
            $('#addImg').hide();
            $('#ajoutSerieForm').show();
        };

        $scope.hideAddSerie = function () {
            $('#addImg').show();
            $('#ajoutSerieForm').hide();
        };
    }]);

