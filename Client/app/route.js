// Les routes
app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/play', {
            controller: 'PlayController as playLoc',
            templateUrl: 'templates/play.html'
        })
        .when('/series', {
            controller: 'SeriesController as seriesLoc',
            templateUrl: 'templates/series.html'
        })
        .when('/game', {
            controller: 'GameController as gameLoc',
            templateUrl: 'templates/game.html'
        })
        .when('/finish', {
            controller: 'FinishController as finishLoc',
            templateUrl: 'templates/finish.html'
        })
        .when('/admin', {
            controller: 'AdminController',
            templateUrl: 'templates/admin.html'
        })
        .otherwise({
            redirectTo: '/play'
        });
}]);

