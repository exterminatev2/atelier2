app.config(['$resourceProvider', function ($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
}]);

app.config(function ($logProvider) {
    $logProvider.debugEnabled(false);
});

